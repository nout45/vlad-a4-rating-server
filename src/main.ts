import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as fs from 'fs';

const httpsOptions = {
  key: fs.readFileSync(
    './../../../../etc/letsencrypt/live/rating-vlada4.tk/privkey.pem',
  ),
  cert: fs.readFileSync(
    './../../../../etc/letsencrypt/live/rating-vlada4.tk/fullchain.pem',
  ),
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { httpsOptions });
  await app.listen(Number(process.env.PORT) || 3000);
}
bootstrap();
