import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './user.model';
import { IncrementUserScoreDto } from './dto/increment-user-score.dto';
import { v4 as uuidv4 } from 'uuid';
import { CreateUserDto } from './dto/create-user.dto';
import sequelize, { Op } from 'sequelize';
import { GetCountRowsDto } from './dto/get-count-rows.dto';
import { UserWeekModel } from './user-week.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User) private userRepository: typeof User,
    @InjectModel(UserWeekModel)
    private userWeekRepository: typeof UserWeekModel,
  ) {}

  async createUser(dto: CreateUserDto) {
    // for (let i = 0; i < 100000; i++) {
    const userId = uuidv4();

    const user = await this.userRepository.create({
      name: dto.name || 'no-name',
      id: userId,
      score: 0,
    });

    await this.userWeekRepository.create({
      id: userId,
      week_score: 0,
    });
    // }

    return user;
  }

  async getAllUsers() {
    return await this.userRepository.findAll();
  }

  async getCountRows(query: GetCountRowsDto, allRows, typeTime = 'all') {
    const where = allRows
      ? undefined
      : {
          [typeTime === 'all' ? 'score' : 'week_score']: {
            [Op.lt]: query['Op.lt'],
          },
        };

    if (typeTime === 'all') {
      return await this.userRepository.count({
        where,
      });
    } else {
      return await this.userWeekRepository.count({
        where,
      });
    }
  }

  async getUserRatingPlace(userId: string, typeTime = 'all') {
    const user =
      typeTime === 'all'
        ? await this.findUser(userId)
        : await this.findWeekUser(userId);
    if (!user) {
      return;
    }

    const allAmountRows = await this.getCountRows(
      { 'Op.lt': 0 },
      true,
      typeTime,
    );

    const amountRowsBelowPlayer = await this.getCountRows(
      // @ts-ignore
      { 'Op.lt': typeTime === 'all' ? user.score : user.week_score },
      false,
      typeTime,
    );

    return { place: allAmountRows - amountRowsBelowPlayer, allAmountRows };
  }

  async findUser(userId) {
    const user = await this.userRepository.findOne({ where: { id: userId } });

    if (user) {
      return user;
    } else {
      return await this.userRepository.create({
        name: 'no-name',
        id: userId,
        score: 0,
      });
    }
  }
  async findWeekUser(userId) {
    const user = await this.userWeekRepository.findOne({
      where: { id: userId },
    });
    if (user) {
      return user;
    } else {
      return await this.userWeekRepository.create({
        id: userId,
        week_score: 0,
      });
    }
  }

  async updateUserScore(user, userWeek, score) {
    const numberScore = Number(score);
    let nextScore = user.score + numberScore;
    let nextWeekScore = userWeek.week_score + numberScore;

    if (score < 0 && user.score + numberScore < 0) {
      nextScore = 0;
    }
    if (score < 0 && userWeek.week_score + numberScore < 0) {
      nextWeekScore = 0;
    }

    console.log('nextWeekScore', nextWeekScore, 'nextScore', nextScore);

    await this.userRepository.update(
      {
        score: nextScore,
      },
      {
        where: {
          id: user.id,
        },
      },
    );

    await this.userWeekRepository.update(
      {
        week_score: nextWeekScore,
      },
      {
        where: {
          id: userWeek.id,
        },
      },
    );
  }

  async incrementUserScore(dto: IncrementUserScoreDto, id) {
    const user = await this.findUser(id);
    const userWeek = await this.findWeekUser(id);

    if (dto.score > 10) {
      return 'Ошибка, максимальное количество 10 очков';
    }

    await this.updateUserScore(user, userWeek, dto.score);

    return await this.getUserInfo(id);
  }

  async getUserInfo(id: string) {
    console.time('getUserInfo');
    const user = await this.findUser(id);
    const weekUser = await this.findWeekUser(id);

    const placeAllTimeData = await this.getUserRatingPlace(id);
    const placeWeekTimeData = await this.getUserRatingPlace(id, 'week');
    console.timeEnd('getUserInfo');

    return {
      placeAllTime: placeAllTimeData.place,
      placeWeekTime: placeWeekTimeData.place,
      amountPlayerAllTime: placeAllTimeData.allAmountRows,
      amountPlayerWeekTime: placeWeekTimeData.allAmountRows,
      score: user.score,
      week_score: weekUser.week_score,
    };
  }

  async getTopPlayers() {
    const usersAllTime = await this.userRepository.findAll({
      limit: 3,
      order: [['score', 'DESC']],
    });

    const usersWeekTimeInit = await this.userWeekRepository.findAll({
      limit: 3,
      order: [['week_score', 'DESC']],
    });

    const idUsersWeekTime = usersWeekTimeInit.map((item) => item.id);

    const usersWeekTimeWithName = await this.userRepository.findAll({
      where: {
        id: {
          [Op.in]: idUsersWeekTime,
        },
      },
    });

    const usersWeekTime = JSON.parse(JSON.stringify(usersWeekTimeInit)).map(
      (item) => {
        // @ts-ignore
        item.name = usersWeekTimeWithName.find(
          (child) => child.id === item.id,
        ).name;

        return item;
      },
    );

    return { usersAllTime, usersWeekTime };
  }

  async updateUserName(userId, nextUserName) {
    if (nextUserName.length > 21 || nextUserName.length < 2) {
      return 'Не удалось обновить имя пользователя';
    }

    await this.userRepository.update(
      { name: nextUserName },
      { where: { id: userId } },
    );

    return await this.getTopPlayers();
  }

  async decrementUserScore(userId, score) {
    if (score > 50 || score < 0) {
      return 'Не удалось уменьшить рейтинг пользователя';
    }

    const user = await this.findUser(userId);
    const userWeek = await this.findWeekUser(userId);

    await this.updateUserScore(user, userWeek, -score);

    return await this.getTopPlayers();
  }
}
