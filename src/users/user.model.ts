import { Column, DataType, Model, Table } from "sequelize-typescript";

export interface UserCreationAttrs {
  id: string;
  blocked: boolean;
  score: number;
  week_score: number;
  name: string;
}

@Table({ tableName: 'users' })
export class User extends Model<User, UserCreationAttrs> {
  @Column({type: DataType.STRING, unique: true, primaryKey: true, allowNull: false})
  id: string;

  @Column({type: DataType.BOOLEAN, defaultValue: false})
  blocked: boolean;

  @Column({type: DataType.INTEGER, defaultValue: 0})
  score: number;

  @Column({type: DataType.INTEGER, defaultValue: 0})
  week_score: number;

  @Column({type: DataType.STRING, allowNull: false, defaultValue: ''})
  name: string;
}
