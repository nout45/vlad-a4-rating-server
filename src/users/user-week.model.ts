import { Column, DataType, Model, Table } from 'sequelize-typescript';

export interface UserCreationAttrs {
  id: string;
  week_score: number;
  name: string;
}

@Table({ tableName: 'users-week' })
export class UserWeekModel extends Model<UserWeekModel, UserCreationAttrs> {
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
    allowNull: false,
  })
  id: string;

  @Column({ type: DataType.INTEGER, defaultValue: 0 })
  week_score: number;
}
