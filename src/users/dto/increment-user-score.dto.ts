export class IncrementUserScoreDto {
  readonly score: number;
  readonly name: string;
}
