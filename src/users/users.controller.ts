import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { IncrementUserScoreDto } from './dto/increment-user-score.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { GetCountRowsDto } from './dto/get-count-rows.dto';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get()
  getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Post()
  createAndReturnUser(@Body() body: CreateUserDto) {
    return this.userService.createUser(body);
  }

  @Get('/get-top-players')
  getTopPlayers() {
    return this.userService.getTopPlayers();
  }

  @Get('/count')
  countRows(@Query() query: GetCountRowsDto) {
    console.log('count');
    return this.userService.getCountRows(query, false);
  }

  @Get('/:id')
  getUser(@Param() params) {
    return this.userService.findUser(params.id);
  }

  @Get('/:id/info')
  getUserInfo(@Param() params) {
    return this.userService.getUserInfo(params.id);
  }

  @Get('/:id/rating-place')
  getUserRatingPlace(@Param() params) {
    return this.userService.getUserRatingPlace(params.id);
  }

  @Put('/:id/increment-score')
  updateUserScore(@Body() body: IncrementUserScoreDto, @Param() params) {
    return this.userService.incrementUserScore(body, params.id);
  }

  @Put('/:id/update-name')
  updateUserName(@Param() params, @Body() body: { name: string }) {
    return this.userService.updateUserName(params.id, body.name);
  }

  @Put('/:id/decrement-score')
  decrementUserScore(@Param() params, @Body() body: { score: number }) {
    return this.userService.decrementUserScore(params.id, body.score);
  }
}
