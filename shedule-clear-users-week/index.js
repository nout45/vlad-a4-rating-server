const schedule = require('node-schedule');
const database = require('./db')

const job = schedule.scheduleJob('0 0 * * 1', function(){
  console.log('The answer to life, the universe, and everything!');

  database.query('DELETE FROM "users-week"').then(data =>
    console.log(data.rows)
  );
});
